<?php

namespace Drupal\druidfire\Spells;

use Drupal\druidfire\SpellBase;

/**
 * Convert a entity revision reference field to entity reference.
 */
class Err2Er extends SpellBase {

  public function schema(array $schema, string $tableName, string $columnName, array $args = []): array {
    $columnName = str_replace('target_id', 'target_revision_id', $columnName);
    $this->schema->dropField($tableName, $columnName);
    unset($schema[$tableName]['fields'][$columnName]);
    unset($schema[$tableName]['indexes'][$columnName]);
    return $schema;
  }

  public function storage(array $yaml, array $args = []): array {
    $yaml['type'] = 'entity_reference';
    return $yaml;
  }

  public function field(array $yaml, array $args = []): array {
    $yaml['field_type'] = 'entity_reference';
    return $yaml;
  }

  public function formDisplay(array $yaml, $fieldName, array $args = []): array {
    if (isset($yaml['content'][$fieldName]['type'])) {
      $yaml['content'][$fieldName]['type'] = str_replace('entity_reference_revisions_', 'entity_reference_', $yaml['content'][$fieldName]['type']);
    }
    return $yaml;
  }

  public function viewDisplay(array $yaml, $fieldName, array $args = []): array {
    return $this->formDisplay($yaml, $fieldName);
  }

}
