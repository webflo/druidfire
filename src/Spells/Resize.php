<?php

namespace Drupal\druidfire\Spells;

use Drupal\druidfire\SpellBase;

/**
 * Resize a text or string field.
 */
class Resize extends SpellBase {

  const DEFAULTSIZE = 1024;

  public function schema(array $schema, string $tableName, string $columnName, array $args = []): array {
    $schema[$tableName]['fields'][$columnName]['length'] = $args['size'] ?? self::DEFAULTSIZE;
    $this->schema->changeField($tableName, $columnName, $columnName, $schema[$tableName]['fields'][$columnName]);
    return $schema;
  }

  public function storage(array $yaml, array $args = []): array {
    $yaml['settings']['max_length'] = $args['size'] ?? self::DEFAULTSIZE;
    return $yaml;
  }

}
