<?php

namespace Drupal\druidfire\Spells;

use Drupal\druidfire\SpellBase;

/**
 * Convert an entity reference revisions field to bricks.
 */
class Err2Bricks extends SpellBase {

  public function schema(array $schema, string $tableName, string $columnName, array $args = []): array {
    $basename = str_replace('_target_id', '', $columnName);
    $new_columns = [
      "{$basename}_depth" => [
        "type" => "int",
        "size" => "tiny",
        "unsigned" => true,
        "not null" => false,
      ],
      "{$basename}_options" => [
        "type" => "blob",
        "size" => "normal",
        "not null" => false,
        "serialize" => true,
      ],
    ];
    foreach ($new_columns as $new_column_name => $new_column) {
      $this->schema->addField($tableName, $new_column_name, $new_column);
    }
    $schema[$tableName]['fields'] = $schema[$tableName]['fields'] + $new_columns;
    return $schema;
  }

  public function storage(array $yaml, array $args = []): array {
    $yaml['type'] = 'bricks_revisioned';
    return $yaml;
  }

  public function field(array $yaml, array $args = []): array {
    $yaml['field_type'] = 'bricks_revisioned';
    return $yaml;
  }

  public function formDisplay(array $yaml, $fieldName, array $args = []): array {
    // Bricks fields can use the bricks_tree_paragraphs widget, but they can
    // also continue to use the standard paragraphs widget, so don't change
    // anything here; new widgets can be put in config as normal.
    return $yaml;
  }

  public function viewDisplay(array $yaml, $fieldName, array $args = []): array {
    if (isset($yaml['content'][$fieldName]['type'])) {
      $yaml['content'][$fieldName]['type'] = 'bricks_revisions_nested';
    }
    return $yaml;
  }


}
